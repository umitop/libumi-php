<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use UmiTop\LibUmi\HelloWorld;

class HelloWorldTest extends TestCase
{
    public function testHelloWorld()
    {
        $obj = new HelloWorld();

        $expected = 'Hello World!';
        $actual = $obj->helloWorld();

        $this->assertEquals($expected, $actual);
    }
}
